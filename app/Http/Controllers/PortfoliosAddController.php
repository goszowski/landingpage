<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class PortfoliosAddController extends Controller
{
    public function execute(Request $request){

        if ($request->isMethod('post')) {
            $input = $request->except('_token');

            $messages = [
                'required'=>'Field :attribute is required',
                'unique'=>'Field :attribute is unique',
            ];

            $validator = Validator::make($input, [
                'name'=>'required|max:255',
                'filter'=>'required',
            ], $messages);
            if ($validator->fails()){
                return redirect()->route('pagesAdd')->withErrors($validator)->withInput();
            }
            if ($request->hasFile('images')){
                $file = $request->file('images');
                $input['images'] = $file->getClientOriginalName();
                $file->move(public_path().'/assets/img', $input['images']);
            }
            $page = new Portfolio();
            $page->fill($input);
            if ($page->save()) {
                return redirect('admin')->with('status', 'Portfolio has been added');
            }
        }

        if (view()->exists('admin.portfolios_add')){
            $data = [
                'title'=>'New portfolio',
            ];
            return view('admin.portfolios_add', $data);
        }
        abort(404);
    }
}
