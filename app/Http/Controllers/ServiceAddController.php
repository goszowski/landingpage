<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class ServiceAddController extends Controller
{
    public function execute(Request $request){

        if ($request->isMethod('post')) {
            $input = $request->except('_token');

            $messages = [
                'required'=>'Field :attribute is required',
                'unique'=>'Field :attribute is unique',
            ];

            $validator = Validator::make($input, [
                'name'=>'required|max:255',
                'text'=>'required',
                'icon'=>'required',
            ], $messages);
            if ($validator->fails()){
                return redirect()->route('serviceAdd')->withErrors($validator)->withInput();
            }
            $service = new Service();
            $service->fill($input);
            if ($service->save()) {
                return redirect('admin')->with('status', 'Service has been added');
            }
        }

        if (view()->exists('admin.services_add')){
            $data = [
                'title'=>'New service',
            ];
            return view('admin.services_add', $data);
        }
        abort(404);
    }
}
