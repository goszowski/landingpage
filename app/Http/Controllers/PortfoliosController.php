<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;

class PortfoliosController extends Controller
{
    public function execute() {
        if(view()->exists('admin.services')){
            $portfolios = Portfolio::all();
            $data = [
                'title'=>'portfolio',
                'portfolios'=>$portfolios,

            ];

            return view('admin.portfolios', $data);
        }
        abort(404);
    }
}
