<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\People;
use App\Portfolio;
use App\Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller
{
    public function execute(Request $request) {
        $messages = [
            'required'=>'Field :attribute is required',
            'email'=>'Email attribute need have @',
        ];
        if($request->isMethod('post')){
            $this->validate($request, [
                'name'=>'required|max:255',
                'email'=>'required|email',
                'text'=>'required',
            ], $messages);
//            dump($request);
            $data = $request->all();


            //mail
//            dd($mail_admin = env('MAIL_ADMIN'));
//            $to_name = 'Богдан Роман';
//            $to_email = 'garmahen22@gmail.com';
//            $data = array('name'=>"Sam Jose", "body" => "Test mail");
//
//            Mail::send('site.email', $data, function($message) use ($to_name, $to_email) {
//                $message->to($to_email, $to_name)
//                    ->subject('Artisans Web Testing Mail');
//                $message->from('test@gmail.com','Artisans Web');
//            });
            Mail::send('site.email', ['data'=>$data], function($mail) use ($data){
                $mail_admin = env('MAIL_ADMIN');
                $mail->to($mail_admin, 'Admin')->subject('TEMA PISMA');
                $mail->from($data['email'], $data['name']);
            });
            return redirect()->route('home')->with('status', 'Email is sending');
//            if($result) {
//                dd($result);
//                return 131;
//                session()->flash('status', ['Message']);
//                return redirect('home')->with('status', 'Message');
//            }
//            dd(session('status'));
            
        }

        $pages = Page::all();
        $portfolio =Portfolio::all(['name','filter','images']);
        $services =Service::all();
        $peoples =People::take(3)->get();
        $tags = DB::table('portfolios')->distinct()->pluck('filter');
        $menu=[];

        foreach ($pages as $page) {
            $item = ['title'=>$page->name,'alias'=>$page->alias];
            array_push($menu, $item);
        }
        $item = ['title'=>'Services','alias'=>'service'];
        array_push($menu, $item);
        $item = ['title'=>'Portfolio','alias'=>'Portfolio'];
        array_push($menu, $item);
        $item = ['title'=>'Team','alias'=>'team'];
        array_push($menu, $item);
        $item = ['title'=>'Contact','alias'=>'contact'];
        array_push($menu, $item);
//        dd($menu);

        return view('site.index', [
            'menu' => $menu,
            'pages'=>$pages,
            'services'=>$services,
            'peoples'=>$peoples,
            'portfolio'=> $portfolio,
            'tags'=>$tags,
        ]);
    }
}
