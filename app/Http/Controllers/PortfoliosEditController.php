<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class PortfoliosEditController extends Controller
{
    public function execute(Portfolio $portfolio,Request $request){

        if ($request->isMethod('delete')) {
            $portfolio->delete();
            return redirect('admin')->with('status', 'Portfolio is deleted');
        }


        if ($request->isMethod('post')) {
            $input = $request->except('_token');
            $messages = [
                'required'=>'Field :attribute is required',
                'unique'=>'Field :attribute is unique',
            ];
            $validator = Validator::make($input,[
                'name'=>'required|max:255',
                'filter'=>'required',
                'images'=>'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->route('pagesEdit', ['portfolio' =>$input['id']])->withErrors($validator)->withInput();
            }

            if ($request->hasFile('images')){
                $file = $request->file('images');
                $input['images'] = $file->getClientOriginalName();
                $file->move(public_path().'/assets/img', $input['images']);
            } else {
                $input['images'] = $input['old_images'];
            }
            unset($input['old_images']);

            $portfolio->fill($input);
            if ($portfolio->update()) {
                return redirect('admin')->with('status', 'Portfolio has been edited');
            }

        }


        $old = $portfolio->toArray();
//        dd($old);
        if (view()->exists('admin.portfolios_edit')) {
            $data = [
                'title' => 'Edit Portfolio - '.$old['name'],
                'data' => $old,
            ];
            return view('admin.portfolios_edit', $data);
        }
    }
}
