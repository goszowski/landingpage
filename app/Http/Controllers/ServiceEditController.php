<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class ServiceEditController extends Controller
{
    public function execute(Service $service,Request $request){
//        $page = Page::find($id);

        if ($request->isMethod('delete')) {
            $service->delete();
            return redirect('admin')->with('status', 'Service is deleted');
        }


        if ($request->isMethod('post')) {
            $input = $request->except('_token');
            $messages = [
                'required'=>'Field :attribute is required',
            ];
            $validator = Validator::make($input,[
                'name'=>'required|max:255',
                'text'=>'required',
                'icon'=>'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->route('serviceEdit', ['services' =>$input['id']])->withErrors($validator)->withInput();
            }

            $service->fill($input);
            if ($service->update()) {
                return redirect('admin')->with('status', 'Page has been edited');
            }

        }


        $old = $service->toArray();
//        dd($old);
        if (view()->exists('admin.services_edit')) {
            $data = [
                'title' => 'Edit Service - '.$old['name'],
                'data' => $old,
            ];
            return view('admin.services_edit', $data);
        }
    }
}
