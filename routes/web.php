<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::group([], function() {
    Route::match(['get', 'post'], '/', ['uses'=>'IndexController@execute', 'as'=>'home']);
    Route::get('/page/{alias}', ['uses'=>'PageController@execute', 'as'=>'page']);
    Route::auth();
});

Route::group(['prefix'=>'admin', 'middleware'=>'auth'], function(){
    // admin/
    Route::get('/', function(){
        if (view()->exists('admin.index')){
            $data = ['title' => 'Admin panel'];
            return view('admin.index', $data);
        }
    });
    // Pages
    Route::group(['prefix'=>'pages'], function(){
        // admin/pages
        Route::get('/', ['uses'=>'PagesController@execute', 'as'=>'pages']);
        // admin/pages/add
        Route::match(['get', 'post'],'/add', ['uses'=>'PagesAddController@execute', 'as'=>'pagesAdd']);
        // admin/edit/{page}
        Route::match(['get', 'post', 'delete'],'/edit/{page}', ['uses'=>'PagesEditController@execute', 'as'=>'pagesEdit']);
    });

    // Portfolios
    Route::group(['prefix'=>'portfolios'], function(){
        Route::get('/', ['uses'=>'PortfoliosController@execute', 'as'=>'portfolios']);
        // admin/portfolios/add
        Route::match(['get', 'post'],'/add', ['uses'=>'PortfoliosAddController@execute', 'as'=>'portfoliosAdd']);
        // admin/edit/{portfolio}
        Route::match(['get', 'post', 'delete'],'/edit/{portfolio}', ['uses'=>'PortfoliosEditController@execute', 'as'=>'portfoliosEdit']);
    });

    //Service
    Route::group(['prefix'=>'service'], function(){
        Route::get('/', ['uses'=>'ServiceController@execute', 'as'=>'service']);
        // admin/service/add
        Route::match(['get', 'post'],'/add', ['uses'=>'ServiceAddController@execute', 'as'=>'serviceAdd']);
        // admin/edit/{portfolio}
        Route::match(['get', 'post', 'delete'],'/edit/{service}', ['uses'=>'ServiceEditController@execute', 'as'=>'serviceEdit']);
    });
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('homes');
